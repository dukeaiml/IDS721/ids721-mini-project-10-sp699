import os
import json
from transformers import pipeline

# Configure environment variables for paths
os.environ["TRANSFORMERS_CACHE"] = "/tmp/transformers/"
os.environ["HF_HOME"] = "/tmp/huggingface/"

# Initialize the model with cache directory settings
model = pipeline(
    "sentiment-analysis",
    model="cardiffnlp/twitter-roberta-base-sentiment-latest",
    cache_dir="/tmp/model/",
)


def lambda_handler(event, context):
    input_text = event.get("input_text", "")
    sentiment_result = model(input_text)

    return {
        "statusCode": 200,
        "body": json.dumps({"sentiment_result": sentiment_result}),
    }
