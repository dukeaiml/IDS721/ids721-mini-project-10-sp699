# Use the Python runtime image provided by AWS
FROM public.ecr.aws/lambda/python:3.8

# Set the working directory
WORKDIR /var/task

# Install the necessary Python libraries
COPY requirements.txt .
RUN python -m pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

# Set environment variables for the Transformers and Hugging Face library cache directories
ENV TRANSFORMERS_CACHE=/tmp/transformers/ HF_HOME=/tmp/huggingface/

# Copy the application code to the container
COPY . .

# Set the CMD to your handler (AWS Lambda function handler)
CMD ["app.lambda_handler"]
