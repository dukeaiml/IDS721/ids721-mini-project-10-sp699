# IDS-721-Cloud-Computing :computer:

## Mini Project 10 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Dockerize Hugging Face Rust Transformer
* Deploy container to AWS Lambda
* Implement query endpoint

## :ballot_box_with_check: Grading Criteria
* __Transformer packaging__ 30%
* __Serverless deployment__ 30%
* __Endpoint functionality__ 30%
* __Documentation__ 20%

## :ballot_box_with_check: Deliverables
* Dockerfile and Rust code
* Screenshot of AWS Lambda
* cURL request against endpoint

## :ballot_box_with_check: Main Progress
1. __Dockerize Hugging Face Rust Transformer__
- Download the Rust Transformer model from the Hugging Face. In this project, `bloom-1b1-q4_0-ggjt.bin` model has been used. Here's the description of the model.
```
Descrition will be updated.
```
- Create a rust lambda function using `bloom` model. 
This function handles incoming HTTP requests, extracts a text input parameter from the query parameters, loads a language model, performs text generation using the input text, and returns the generated text as an HTTP response.
```rust
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use std::collections::HashMap;
use tracing_subscriber::fmt::Subscriber;
use tracing_subscriber::EnvFilter;
use anyhow::Result;
use std::convert::Infallible;
use std::path::PathBuf;
use llm::{InferenceFeedback, InferenceParameters, InferenceRequest, InferenceResponse};

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let query_params = event.uri().query().unwrap_or("");
    let query_map: HashMap<String, String> = serde_urlencoded::from_str(query_params).expect("Invalid query");
    let input_res = match query_map.get("text") {
        Some(input) => input,
        None => {
            let resp = Response::builder()
                .status(500)
                .header("content-type", "text/html")
                .body(("invalid format").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    // Ensure to use the correct model architecture enum and path
    let model = llm::load_dynamic(
        Some(llm::ModelArchitecture::GptNeoX),
        &PathBuf::from("/bloom-1b1-q4_0-ggjt.bin"),
        llm::TokenizerSource::Embedded,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;
    
    let mut session = model.start_session(Default::default());
    let mut response_text = String::new();
    let _ = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &InferenceRequest {
            prompt: llm::Prompt::from(input_res), // Adjust according to actual Prompt creation method
            parameters: &InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(8),
        },
        &mut Default::default(),
        |response| match response {
            InferenceResponse::PromptToken(token) | InferenceResponse::InferredToken(token) => {
                response_text.push_str(&token);
                Ok(InferenceFeedback::Continue)
            }
            _ => Ok(InferenceFeedback::Continue),
        },
    );

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::from(response_text))
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let subscriber = Subscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");

    run(service_fn(function_handler)).await
}
```

2. Deploy container to AWS Lambda