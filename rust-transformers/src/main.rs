use lambda_http::{run, service_fn, Body, Error, Request, Response};
use std::collections::HashMap;
use tracing_subscriber::fmt::Subscriber;
use tracing_subscriber::EnvFilter;
use anyhow::Result;
use std::convert::Infallible;
use std::path::PathBuf;
use llm::{InferenceFeedback, InferenceParameters, InferenceRequest, InferenceResponse};

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let query_params = event.uri().query().unwrap_or("");
    let query_map: HashMap<String, String> = serde_urlencoded::from_str(query_params).expect("Invalid query");
    let input_res = match query_map.get("text") {
        Some(input) => input,
        None => {
            let resp = Response::builder()
                .status(500)
                .header("content-type", "text/html")
                .body(("invalid format").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    // Ensure to use the correct model architecture enum and path
    let model = llm::load_dynamic(
        Some(llm::ModelArchitecture::GptNeoX),
        &PathBuf::from("/bloom-1b1-q4_0-ggjt.bin"),
        llm::TokenizerSource::Embedded,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;
    
    let mut session = model.start_session(Default::default());
    let mut response_text = String::new();
    let _ = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &InferenceRequest {
            prompt: llm::Prompt::from(input_res), // Adjust according to actual Prompt creation method
            parameters: &InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(8),
        },
        &mut Default::default(),
        |response| match response {
            InferenceResponse::PromptToken(token) | InferenceResponse::InferredToken(token) => {
                response_text.push_str(&token);
                Ok(InferenceFeedback::Continue)
            }
            _ => Ok(InferenceFeedback::Continue),
        },
    );

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::from(response_text))
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let subscriber = Subscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");

    run(service_fn(function_handler)).await
}
